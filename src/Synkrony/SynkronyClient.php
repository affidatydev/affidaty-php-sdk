<?php
/**
 * SynkronyClient class
 */

namespace Synkrony;

use Synkrony\Exceptions\SynkronySDKException;
use Synkrony\HttpClients\CurlHttpClient;
use Synkrony\HttpClients\HttpClientInterface;
use Synkrony\Responses\BalanceResponse;
use Synkrony\Responses\BaseResponse;
use Synkrony\Responses\ListResponse;
use Synkrony\Responses\ResponseFactory;
use Synkrony\Responses\ResponseInterface;
use Synkrony\Responses\SimpleResponse;

/**
 * Class SynkronyClient
 *
 * @package Synkrony
 */
class SynkronyClient
{
    /**
     * @const string Production URL.
     */
    const SYNKRONY_URL = 'https://synkrony.affidaty.io';

    /**
     * @const string Test URL.
     */
    const SYNKRONY_URL_TEST = 'https://devsynkrony.affidaty.io';

    /**
     * @const int The timeout in seconds for a normal request.
     */
    const DEFAULT_REQUEST_TIMEOUT = 60;

    /**
     * @var bool Toggle to use test url.
     */
    protected $enableTestMode = false;

    /**
     * @var HttpClientInterface HTTP client handler.
     */
    protected $httpClientHandler;

    /**
     * Instantiates a new SynkronyClient object.
     *
     * @param HttpClientInterface|null      $httpClientHandler
     * @param boolean                       $enableTest
     *
     * @throws SynkronySDKException
     */
    public function __construct(HttpClientInterface $httpClientHandler = null, $enableTest = false)
    {
        $this->httpClientHandler = $httpClientHandler ?: $this->detectHttpClientHandler();
        //$this->enableTestMode = $enableTest;
    }

    /**
     * Sets the HTTP client handler.
     *
     * @param HttpClientInterface $httpClientHandler
     */
    public function setHttpClientHandler(HttpClientInterface $httpClientHandler)
    {
        $this->httpClientHandler = $httpClientHandler;
    }

    /**
     * Returns the HTTP client handler.
     *
     * @return HttpClientInterface
     */
    public function getHttpClientHandler()
    {
        return $this->httpClientHandler;
    }

    /**
     * Detects which HTTP client handler to use.
     *
     * @return HttpClientInterface
     *
     * @throws SynkronySDKException
     */
    public function detectHttpClientHandler()
    {
        if(extension_loaded('curl'))
            return new CurlHttpClient();

        throw new SynkronySDKException('no http client available', null);

    }

    /**
     * Toggle test mode.
     *
     * @param boolean $testMode
     */
    public function enableTestMode($testMode = true)
    {
        $this->enableTestMode = $testMode;
    }

    /**
     * Prepares the request for sending to the client handler.
     *
     * @param SynkronyRequest   $request
     * @param array             $other      other configurations parameter
     *
     * @return array
     */
    public function prepareRequestMessage(SynkronyRequest $request, $other = null)
    {
        if(isset($other) && isset($other['endpoint'])) {
            $url = $this->enableTestMode ? self::SYNKRONY_URL_TEST: self::SYNKRONY_URL;
            $url.= $other['endpoint'];
        } else {
            $url = $this->enableTestMode ? $request->getApp()->getTestUrl(): $request->getApp()->getProductionUrl();
        }

        // If we're sending files they should be sent as multipart/form-data
        if ('json' == $request->getParamsType()) {
            $requestBody = $request->getJsonBody();
            $request->setHeaders([
                'Content-Type' => 'application/json',
            ]);
        } else {
            $requestBody = $request->getUrlEncodedBody();
            $request->setHeaders([
                'Content-Type' => 'application/x-www-form-urlencoded',
            ]);
        }
        $request->setHeaders(['applicationid' => $request->getApp()->getAppId()]);

        return [
            $url,
            $request->getMethod(),
            $request->getHeaders(),
            $requestBody->getBody(),
        ];
    }

    /**
     * Makes the request and returns the result.
     *
     * @param SynkronyRequest   $request
     * @param string            $type
     * @param array             $other
     *
     * @return BaseResponse|SimpleResponse|BalanceResponse|ListResponse
     *
     * @throws SynkronySDKException
     */
    public function sendRequest(SynkronyRequest $request, $type, $other = null)
    {
        list($url, $method, $headers, $body) = $this->prepareRequestMessage($request, $other);

        $timeOut = 70;
        // Should throw `SynkronySDKException` exception on HTTP client error.
        // Don't catch to allow it to bubble up.
        $rawResponse = $this->httpClientHandler->send($url, $method, $body, $headers, $timeOut);

        $returnResponse = ResponseFactory::createResponse(
            $type,
            $request,
            $rawResponse->getBody(),
            $rawResponse->getHttpResponseCode(),
            $rawResponse->getHeaders()
        );

        if (true === $returnResponse->isError()) {
            throw $returnResponse->getThrownException();
        }

        return $returnResponse;
    }
}
