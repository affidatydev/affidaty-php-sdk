<?php
/**
 * Main Response class
 */
namespace Synkrony;

use Synkrony\Exceptions\SynkronySDKException;
use Synkrony\Utils\BodyWrapper;

/**
 * Class SynkronyResponse
 *
 * @package Synkrony
 */
class SynkronyResponse
{
    /**
     * @var int The HTTP status code response.
     */
    protected $httpStatusCode;

    /**
     * @var array The headers returned.
     */
    protected $headers;

    /**
     * @var string The raw body of the response.
     */
    protected $body;

    /**
     * @var array The decoded body of the response.
     */
    protected $decodedBody = [];

    /**
     * @var SynkronyRequest The original request that returned this response.
     */
    protected $request;

    /**
     * @var SynkronySDKException The exception thrown by this request.
     */
    protected $thrownException;

    /**
     * Creates a new Response entity.
     *
     * @param SynkronyRequest $request
     * @param string|null     $body
     * @param int|null        $httpStatusCode
     * @param array|null      $headers
     * @param string|null     $type
     *
     * @throws SynkronySDKException
     */
    public function __construct(SynkronyRequest $request, $body = null, $httpStatusCode = null, array $headers = [], string $type = null)
    {
        $this->request = $request;
        $this->body = $body;
        $this->httpStatusCode = $httpStatusCode;
        $this->headers = $headers;

        $this->decodeBody();
    }

    /**
     * Return the original request that returned this response.
     *
     * @return SynkronyRequest
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Return the SynkronyApp entity used for this response.
     *
     * @return SynkronyApp
     */
    public function getApp()
    {
        return $this->request->getApp();
    }

    /**
     * Return the HTTP status code for this response.
     *
     * @return int
     */
    public function getHttpStatusCode()
    {
        return $this->httpStatusCode;
    }

    /**
     * Return the HTTP headers for this response.
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Return the raw body response.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Return the decoded body response.
     *
     * @return array
     */
    public function getDecodedBody()
    {
        return $this->decodedBody;
    }

    /**
     * Returns true if returned an error message.
     *
     * @return boolean
     *
     * @throws SynkronySDKException
     */
    public function isError()
    {
        return $this->decodedBody['error'];
    }

    /**
     * Throws the exception.
     *
     * @throws SynkronySDKException
     */
    public function throwException()
    {
        throw $this->thrownException;
    }

    /**
     * Instantiates an exception to be thrown later.
     *
     * @param string $message
     * @param array $exceptionData
     */
    public function makeException($message = null, $exceptionData = null)
    {
        if(is_null($message)){
            $message = 'Something went wrong';
        }

        $this->thrownException = new SynkronySDKException($message, $exceptionData);
    }

    /**
     * Returns the exception that was thrown for this request.
     *
     * @return SynkronySDKException|null
     */
    public function getThrownException()
    {
        return $this->thrownException;
    }

    /**
     * Convert the raw response into an array if possible.
     *
     * @throws SynkronySDKException
     */
    public function decodeBody()
    {
        $this->decodedBody = json_decode($this->body, true);

        if (is_bool($this->decodedBody)) {
            $this->decodedBody = ['success' => $this->decodedBody];
        }

        if (!is_array($this->decodedBody)) {
            $this->decodedBody = [];
        }

        if(empty($this->decodedBody)) {
            throw new SynkronySDKException('Can\'t connect to the server..', null);
        }

        if (true === $this->isError() || null === $this->isError()) {

            if(isset($this->decodedBody['message'])){
                $exceptionData = (isset($this->decodedBody['exceptionData']))? $this->decodedBody['exceptionData']: null;
                $this->makeException($this->decodedBody['message'], $exceptionData);
            }
            else
                $this->makeException();
        }
    }
}
