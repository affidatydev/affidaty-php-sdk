<?php
/**
 * This class handle the minimal sdk response.
 */
namespace Synkrony\Responses;

use Synkrony\Exceptions\SynkronySDKException;
use Synkrony\SynkronyRequest;
use Synkrony\SynkronyResponse;

/**
 * Class BaseResponse
 *
 * @package Synkrony
 */
class BaseResponse extends SynkronyResponse implements ResponseInterface
{
    /**
     * Creates a new BaseResponse entity.
     *
     * @param SynkronyRequest   $request            SynkronyRequest object
     * @param string            $body               Response body
     * @param int               $httpStatusCode     Response http status code
     * @param array             $headers            Response headers
     * @param string            $type               Response type
     *
     * @throws SynkronySDKException
     */
    public function __construct(SynkronyRequest $request, string $body = null, int $httpStatusCode = null, array $headers = [], string $type = null)
    {
        parent::__construct($request, $body, $httpStatusCode, $headers, $type);
    }

    /**
     * Return the transaction id
     *
     * @return string
     */
    public function getResult()
    {
        return $this->getDecodedBody()['result'];
    }

    /**
     * Returns true if returned an error message.
     *
     * @return boolean
     */
    public function isError()
    {
        return $this->getDecodedBody()['error'];
    }
}
