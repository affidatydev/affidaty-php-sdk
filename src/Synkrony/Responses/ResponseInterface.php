<?php

/**
 * Interface for response objects
 */

namespace Synkrony\Responses;

/**
 * Interface ResponseInterface
 *
 * @package Synkrony
 */
interface ResponseInterface
{
    /**
     * Get the result section of the response message
     */
    public function getResult();

    /**
     * Check if there is any error
     */
    public function isError();
}
