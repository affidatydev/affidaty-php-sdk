<?php
/**
 * Response factory class
 */
namespace Synkrony\Responses;

use Synkrony\Exceptions\SynkronySDKException;

/**
 * Class ResponseFactory
 *
 * @package Synkrony
 */
class ResponseFactory
{
    /**
     * Return the typed response object
     *
     * @param string $type                          Response type
     * @param \Synkrony\SynkronyRequest $request    Request sent
     * @param string $body                          Response body
     * @param int $httpCode                         Response http client code
     * @param array $header                         Response header
     *
     * @return BaseResponse|SimpleResponse|BalanceResponse|ListResponse
     *
     * @throws SynkronySDKException
     */
    public static function createResponse($type, $request, $body, $httpCode, $header)
    {
        if('base' === $type){
            return new BaseResponse($request, $body, $httpCode, $header);
        }

        if('simple' === $type){
            return new SimpleResponse($request, $body, $httpCode, $header);
        }

        if('balance' === $type) {
            return new BalanceResponse($request, $body, $httpCode, $header);
        }

        if('list' === $type) {
            return new ListResponse($request, $body, $httpCode, $header);
        }
    }
}
