<?php
/**
 * BalanceResponse class.
 */
namespace Synkrony\Responses;

use Synkrony\Exceptions\SynkronySDKException;
use Synkrony\SynkronyRequest;
use Synkrony\SynkronyResponse;

/**
 * Class BalanceResponse
 *
 * @package Synkrony
 */
class BalanceResponse extends SynkronyResponse implements ResponseInterface
{
    /**
     * Creates a new BalanceResponse entity.
     *
     * @param SynkronyRequest   $request            SynkronyRequest object
     * @param string            $body               Response body
     * @param int               $httpStatusCode     Response http status code
     * @param array             $headers            Response headers
     * @param string            $type               Response type
     *
     * @throws SynkronySDKException
     */
    public function __construct(SynkronyRequest $request, string $body = null, int $httpStatusCode = null, array $headers = [], string $type = null)
    {
        parent::__construct($request, $body, $httpStatusCode, $headers, $type);
    }

    /**
     * Return formatted balance result
     *
     * @return array
     */
    public function getResult()
    {
        return [ 'result' =>
            [
                'disp' => $this->decodedBody['disp'][0]['result'][0],
                'cont' => $this->decodedBody['cont'][0]['result'][0]
            ]
        ];
    }

    /**
     * Return "disp" section of the response message
     *
     * @return array
     */
    public function getBalanceAvailable()
    {
        return (!empty($this->decodedBody['disp'][0]['result'])) ? $this->decodedBody['disp'][0]['result'][0] : NULL;
    }

    /**
     * Return "cont" section of the response message
     *
     * @return array
     */
    public function getBalanceAccount()
    {
        return (!empty($this->decodedBody['cont'][0]['result'])) ? $this->decodedBody['cont'][0]['result'][0] : NULL;
    }

    /**
     * Returns true if an error message is returned.
     *
     * @return boolean
     *
     * @throws SynkronySDKException
     */
    public function isError()
    {
        return $this->decodedBody['disp'][0]['error'] || $this->decodedBody['cont'][0]['error'];
    }
}
