<?php
/**
 * ResponseBody class
 */

namespace Synkrony\Responses;

use Synkrony\Utils\Utils;
/**
 * Class ResponseBody
 *
 * @package Synkrony
 */

class ResponseBody
{
    /**
     * Balance section of response message
     *
     * @var $balance
     */
    protected $balance;
    /**
     * Items section of response message
     *
     * @var $items
     */
    protected $items;
    /**
     * Saved data section of response message
     *
     * @var $data
     */
    protected $data;
    /**
     * Transaction confirmations
     *
     * @var $confirmations
     */
    protected $confirmations;
    /**
     * Transaction id
     *
     * @var $transactionId
     */
    protected $transactionId;
    /**
     * Validity of the transaction
     *
     * @var $valid
     */
    protected $valid;


    /**
     * Create a new ResponseBody class
     *
     * @param array $responseBody
     */
    public function __construct(array $responseBody)
    {
        $this->balance = $responseBody['balance'];
        $this->items = $responseBody['items'];
        $this->data = $responseBody['data'];
        $this->confirmations = $responseBody['confirmations'];
        $this->transactionId = $responseBody['txid'];
        $this->valid = $responseBody['valid'];
    }

    /**
     * Return the balance section of the response
     *
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Return the items section of the response
     *
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Return decoded saved data
     *
     * @return mixed
     */
    public function getData()
    {
        $data = [];
        foreach ($this->data as $d){
            $data[] = json_decode(Utils::decodeString($d));
        }
        return $data;
    }

    /**
     * Get the confirmations number
     *
     * @return mixed
     */
    public function getConfirmations()
    {
        return $this->confirmations;
    }

    /**
     * Return the transaction id
     *
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Return the transaction validity
     *
     * @return mixed
     */
    public function getValid()
    {
        return $this->valid;
    }
}
