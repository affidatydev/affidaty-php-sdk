<?php
/**
 * ListResponse class.
 */
namespace Synkrony\Responses;

use Synkrony\Exceptions\SynkronySDKException;
use Synkrony\SynkronyRequest;
use Synkrony\SynkronyResponse;
/**
 * Class ListResponse
 *
 * @package Synkrony
 */
class ListResponse extends SynkronyResponse implements ResponseInterface
{
    /**
     * Creates a new BaseResponse entity.
     *
     * @param SynkronyRequest   $request            SynkronyRequest object
     * @param string            $body               Response body
     * @param int               $httpStatusCode     Response http status code
     * @param array             $headers            Response headers
     * @param string            $type               Response type
     *
     * @throws SynkronySDKException
     */
    public function __construct(SynkronyRequest $request, string $body = null, int $httpStatusCode = null, array $headers = [], string $type = null)
    {
        parent::__construct($request, $body, $httpStatusCode, $headers, $type);
    }

    /**
     * Return a list of ResponseBody
     *
     * @return ResponseBody[]   an array of ResponseBody enitites
     */
    public function getResult()
    {
        $responses = [];
        foreach ($this->getDecodedBody()['result'] as $responseBody){
            array_push($responses, new ResponseBody($responseBody));
        }
        return $responses;
    }

    /**
     * Returns true if returned an error message
     *
     * @return boolean
     */
    public function isError()
    {
        return $this->getDecodedBody()['error'];
    }
}
