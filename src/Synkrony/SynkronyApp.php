<?php

/**
 * This class contains some information about the developer and the applications itself
 */

namespace Synkrony;

use Synkrony\Exceptions\SynkronySDKException;

/**
 * Class SynkronyApp
 *
 * @package Synkrony
 */
class SynkronyApp
{
    /**
     * @var string The application key.
     */
    protected $application_key;

    /**
     * @var string The application id.
     */
    protected $application_id;

    /**
     * @var string The production url.
     */
    protected $production_url;

    /**
     * Creates a new SynkronyApp entity.
     *
     * @param array $config
     *
     * @throws SynkronySDKException
     */
    public function __construct($config)
    {
        if (!is_string($config['application_key'])) {
            throw new SynkronySDKException('The "application_key" must be formatted as a string.', null);
        }

        if (!is_string($config['application_id'])) {
            throw new SynkronySDKException('The "application_id" must be formatted as a string.', null);
        }

        $this->application_key = $config['application_key'];
        $this->application_id = $config['application_id'];
        //$this->test_url = $config['test_url'];
        $this->production_url = $config['production_url'];
    }

    /**
     * Returns the app Key.
     *
     * @return string
     */
    public function getAppKey()
    {
        return $this->application_key;
    }

    /**
     * Returns the app Id.
     *
     * @return string
     */
    public function getAppId()
    {
        return $this->application_id;
    }

    /**
     * Returns the test url
     *
     * @return string
     */
    public function getTestUrl()
    {
        return $this->test_url;
    }

    /**
     * Returns the production url
     *
     * @return string
     */
    public function getProductionUrl()
    {
        return $this->production_url;
    }
}
