<?php
/**
 * SynkronyRequest class
 */
namespace Synkrony;

use Synkrony\Http\RequestBodyJsonEncoded;
use Synkrony\Http\RequestBodyUrlEncoded;
use Synkrony\Exceptions\SynkronySDKException;

/**
 * Class Request
 *
 * @package Synkrony
 */
class SynkronyRequest
{
    /**
     * @var SynkronyApp The Synkrony app entity.
     */
    protected $app;

    /**
     * @var string The HTTP method for this request.
     */
    protected $method;

    /**
     * @var array The parameters to send with this request.
     */
    protected $params = [];

    /**
     * @var string The parameters type
     */
    protected $params_type;

    /**
     * @var array Request headers
     */
    protected $headers = [];

    /**
     * Creates a new Request entity.
     *
     * @param SynkronyApp       $app
     * @param string            $method
     * @param array             $params
     * @param string            $params_type
     *
     * @throws SynkronySDKException
     */
    public function __construct(SynkronyApp $app = null, $method = null, array $params = [], $params_type = null)
    {
        $this->setApp($app);
        $this->setMethod($method);
        $this->setParams($params);
        $this->setParamsType($params_type);
    }

    /**
     * Set the SynkronyApp entity used for this request.
     *
     * @param SynkronyApp|null $app
     */
    public function setApp(SynkronyApp $app = null)
    {
        $this->app = $app;
    }

    /**
     * Return the SynkronyApp entity used for this request.
     *
     * @return SynkronyApp
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * Set the HTTP method for this request.
     *
     * @param string
     */
    public function setMethod($method)
    {
        $this->method = strtoupper($method);
    }

    /**
     * Return the HTTP method for this request.
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Validate that the HTTP method is set.
     *
     * @throws SynkronySDKException
     */
    public function validateMethod()
    {
        if (!$this->method) {
            throw new SynkronySDKException('HTTP method not specified.', null);
        }

        if (!in_array($this->method, ['GET', 'POST'])) {
            throw new SynkronySDKException('Invalid HTTP method specified.', null);
        }
    }

    /**
     * Set the params for this request.
     *
     * @param array $params
     *
     * @return SynkronyRequest
     */
    public function setParams(array $params = [])
    {
        $this->params = array_merge($this->params, $params);

        return $this;
    }

    /**
     * Set the parameters type
     *
     * @param string $params_type
     */
    public function setParamsType($params_type)
    {
        $this->params_type = $params_type;
    }

    /**
     * Get the parameters type
     *
     * @return string
     */
    public function getParamsType()
    {
        return $this->params_type;
    }


    /**
     * Returns the body of the request as json string.
     *
     * @return RequestBodyJsonEncoded
     */
    public function getJsonBody()
    {
        $params = $this->getPostParams();

        return new RequestBodyJsonEncoded($params);
    }

    /**
     * Returns the body of the request as URL-encoded.
     *
     * @return RequestBodyUrlEncoded
     */
    public function getUrlEncodedBody()
    {
        $params = $this->getPostParams();

        return new RequestBodyUrlEncoded($params);
    }

    /**
     * Generate and return the params for this request.
     *
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Only return params on POST requests.
     *
     * @return array
     */
    public function getPostParams()
    {
        if ($this->getMethod() === 'POST') {
            return $this->getParams();
        }

        return [];
    }

    /**
     * Generate and return the headers for this request.
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Set the headers for this request.
     *
     * @param array $headers
     */
    public function setHeaders(array $headers)
    {
        $this->headers = array_merge($this->headers, $headers);
    }
}
