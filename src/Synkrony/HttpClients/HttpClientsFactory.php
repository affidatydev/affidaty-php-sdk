<?php

/**
 * Http clients factory class
 */

namespace Synkrony\HttpClients;

use InvalidArgumentException;
use Exception;

/**
 * Class HttpClientsFactory
 *
 * @package Synkrony
 */
class HttpClientsFactory
{
    /**
     * Creates a new HttpClientsFactory entity.
     */
    private function __construct()
    {
        // a factory constructor should never be invoked
    }

    /**
     * HTTP client generation.
     *
     * @param HttpClientInterface|string|null $handler
     *
     * @throws Exception                If the cURL extension is not available (if required).
     * @throws InvalidArgumentException If the http client handler isn't "curl".
     *
     * @return HttpClientInterface
     */
    public static function createHttpClient($handler)
    {
        if (!$handler) {
            return self::detectDefaultClient();
        }

        if ($handler instanceof HttpClientInterface) {
            return $handler;
        }

        if ('curl' === $handler) {
            if (!extension_loaded('curl')) {
                throw new Exception('The cURL extension must be loaded in order to use the "curl" handler.');
            }

            return new CurlHttpClient();
        }
        throw new InvalidArgumentException('The http client handler must be set to "curl" or an instance of Synkrony\HttpClients\SynkronyHttpClientInterface');
    }

    /**
     * Detect default HTTP client.
     *
     * @return HttpClientInterface
     */
    private static function detectDefaultClient()
    {
        if (extension_loaded('curl')) {
            return new CurlHttpClient();
        }
    }
}
