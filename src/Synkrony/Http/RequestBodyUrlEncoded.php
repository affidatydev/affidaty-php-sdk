<?php

/**
 * Transform request parameter to URL encoded string
 */

namespace Synkrony\Http;

/**
 * Class RequestBodyUrlEncoded
 *
 * @package Synkrony
 */
class RequestBodyUrlEncoded implements RequestBodyInterface
{
    /**
     * @var array The parameters to send with this request.
     */
    protected $params = [];

    /**
     * Creates a new UrlEncodedBody entity.
     *
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Return url encoded parameter version
     *
     * @return string
     */
    public function getBody()
    {
        return http_build_query($this->params, null, '&');
    }
}
