<?php
/**
 * RequestBodyJsonEncoded class
 */
namespace Synkrony\Http;

/**
 * Class RequestBodyJsonEncoded
 *
 * @package Synkrony
 */
class RequestBodyJsonEncoded implements RequestBodyInterface
{
    /**
     * @var array The parameters to send with this request.
     */
    protected $params = [];

    /**
     * Creates a new JsonEncodedBody entity.
     *
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Return the encoded body
     *
     * @return string
     */
    public function getBody()
    {
        return json_encode($this->params);
    }
}
