<?php
/**
 * RequestBodyInterface interface
 */
namespace Synkrony\Http;

/**
 * Interface RequestBodyInterface
 *
 * @package Synkrony
 */
interface RequestBodyInterface
{
    /**
     * Get the body of the request to send
     *
     * @return string
     */
    public function getBody();
}
