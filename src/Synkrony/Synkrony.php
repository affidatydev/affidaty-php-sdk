<?php

/**
 * Synkrony SDK super class
 */

namespace Synkrony;

use Synkrony\Exceptions\SynkronySDKException;
use Synkrony\HttpClients\HttpClientsFactory;
use Synkrony\Responses\BalanceResponse;
use Synkrony\Responses\BaseResponse;
use Synkrony\Responses\ListResponse;
use Synkrony\Responses\SimpleResponse;
use Synkrony\Utils\Utils;

/**
 * Synkrony Class
 */
class Synkrony
{
    /**
     * @var SynkronyApp The SynkronyApp entity.
     */
    protected $app;
    /**
     * @var SynkronyClient The Synkrony client service.
     */
    protected $client;

    /**
     * Instantiates a new Synkrony super-class object.
     *
     * @param array $config
     *
     * @throws SynkronySDKException
     */
    public function __construct($config = [])
    {
        $config = array_merge([
            //'test_mode' => true,
            'http_client_handler' => null,
        ], $config);

        if (!$config['application_key'] || empty($config['application_key'])) {
            throw new SynkronySDKException('Required "application_key" not supplied in config', null);
        }

        if (!$config['application_id'] || empty($config['application_id'])) {
            throw new SynkronySDKException('Required "application_id" not supplied in config', null);
        }

        $this->app = new SynkronyApp($config);
        $this->client = new SynkronyClient(
            HttpClientsFactory::createHttpClient($config['http_client_handler'])
        );
    }

    /**
     * Returns the SynkronyApp entity.
     *
     * @return SynkronyApp
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * Returns the SynkronyClient service.
     *
     * @return SynkronyClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Return the wallet associated balance
     *
     * @return BalanceResponse
     * @throws SynkronySDKException
     */
    public function balances()
    {
        $token = $this->getToken();
        $infos = $token->getResult();

        $requestBcParams = [
            'exec' => 'getb',
            'waddr' => $infos['connectionInfo']['walletaddress'],
            'nodoint' => $infos['connectionInfo']['ownerip'],
            'token' => $infos['totp'],
            'ts' => $infos['timestamp']
        ];

        $request = new SynkronyRequest(
            $this->app,
            'POST',
            $requestBcParams,
            'url'
        );

        $response = $this->getClient()->sendRequest($request, 'balance');
        return $response;
    }

    /**
     * Transfer asset from a wallet to another one
     *
     * @param string $sender
     * @param float $amount
     * @param string $asset
     *
     * @return BaseResponse
     * @throws SynkronySDKException
     */
    public function transfers($sender, $amount, $asset)
    {
        $token = $this->getToken(true, ['sender' => $sender, 'asset' => $asset]);
        $infos = $token->getResult();

        $requestBcParams = [
            'exec' => 'sendData',
            'sender' => $infos['senderConnectionInfo']['walletaddress'],
            'receiver' => $infos['connectionInfo']['walletaddress'],
            'assetqty' => $amount,
            'nodoint' => $infos['connectionInfo']['ownerip'],
            'asset' => $asset,
            'data' => '',
            'token' => $infos['totp'],
            'ts' => $infos['timestamp']
        ];

        $request = new SynkronyRequest(
            $this->app,
            'POST',
            $requestBcParams,
            'url'
        );

        $response = $this->getClient()->sendRequest($request, 'base');
        return $response;
    }

    /**
     * Return the wallet transaction list
     *
     * @param int $rows
     * @param int $skip
     *
     * @return ListResponse
     * @throws SynkronySDKException
     */
    public function transactions($rows = 10, $skip = 0)
    {
        $token = $this->getToken();
        $infos = $token->getResult();

        $requestBcParams = [
            'exec' => 'lstw',
            'waddr' => $infos['connectionInfo']['walletaddress'],
            'token' => $infos['totp'],
            'nodoint' => $infos['connectionInfo']['ownerip'],
            'rows' => $rows,
            'skip' => $skip,
            'ts' => $infos['timestamp']
        ];

        $request = new SynkronyRequest(
            $this->app,
            'POST',
            $requestBcParams,
            'url'
        );

        $response = $this->getClient()->sendRequest($request, 'list');
        return $response;
    }

    /**
     * Show some transaction informations
     *
     * @param string $transaction
     *
     * @return SimpleResponse
     * @throws SynkronySDKException
     */
    public function details($transaction)
    {
        $token = $this->getToken();
        $infos = $token->getResult();

        $requestBcParams = [
            'exec' => 'lstx',
            'waddr' => $infos['connectionInfo']['walletaddress'],
            'token' => $infos['totp'],
            'nodoint' => $infos['connectionInfo']['ownerip'],
            'txid' => $transaction,
            'ts' => $infos['timestamp']
        ];

        $request = new SynkronyRequest(
            $this->app,
            'POST',
            $requestBcParams,
            'url'
        );

        $response = $this->getClient()->sendRequest($request, 'simple');
        return $response;
    }

    /**
     * Save some wallet informations
     *
     * @param string $data json string | array
     *
     * @return BaseResponse
     * @throws SynkronySDKException
     */
    public function save($data)
    {
        $params = $this->checkAndConvertSaveData($data);
        $token = $this->getToken();
        $infos = $token->getResult();

        $requestBcParams = [
            'exec' => 'sendData',
            'sender' => $infos['connectionInfo']['walletaddress'],
            'receiver' => $infos['connectionInfo']['addressdb'],
            'nodoint' => $infos['connectionInfo']['ownerip'],
            'token' => $infos['totp'],
            'asset' => 'bitbel',
            'assetqty' => '0.00',
            'ts' => $infos['timestamp'],
            'data' => $params
        ];

        $request = new SynkronyRequest(
            $this->app,
            'POST',
            $requestBcParams,
            'url'
        );

        $response = $this->getClient()->sendRequest($request, 'base');
        return $response;
    }

    /**
     * Register new user in Synkrony
     *
     * @param array $userData Array of user data information
     *
     * @return BaseResponse
     * @throws SynkronySDKException
     */
    public function register($userData)
    {
        $userData['r'] = 'local';
        $request = new SynkronyRequest(
            $this->app,
            'POST',
            $userData,
            'json'
        );

        $request->setHeaders(['apikey' => $this->getApp()->getAppKey()]);
        $response = $this->getClient()->sendRequest($request, 'base', ['endpoint' => '/api/auth/registration']);
        return $response;
    }


    /**
     * Login user
     *
     * @param array $credential
     *
     * @return SimpleResponse
     * @throws SynkronySDKException
     */
    public function login($credential)
    {
        $credential['onboarding'] = true;
        $request = new SynkronyRequest(
            $this->app,
            'POST',
            $credential,
            'json'
        );

        $request->setHeaders(['apikey' => $this->getApp()->getAppKey()]);
        $response = $this->getClient()->sendRequest($request, 'simple', ['endpoint' => '/api/auth/login']);
        return $response;
    }

    /**
     * Refund wallet with Synk
     *
     * @param array $data
     *
     * @return BaseResponse
     * @throws SynkronySDKException
     */
    public function refundSynk($data)
    {
        $request = new SynkronyRequest(
            $this->app,
            'POST',
            $data,
            'json'
        );

        $request->setHeaders(['apikey' => $this->getApp()->getAppKey()]);
        $response = $this->getClient()->sendRequest($request, 'base', ['endpoint' => '/api-sdk/payment/refund']);
        return $response;
    }

    /**
     * Transfer Synk
     *
     * @param string $receiver
     * @param float $amount
     *
     * @return BaseResponse
     * @throws SynkronySDKException
     */
    public function transferSynk($receiver, $amount)
    {
        $token = $this->getToken(true, ['receiver' => $receiver, 'asset' => 'synk']);
        $infos = $token->getResult();

        $requestBcParams = [
            'exec' => 'sendData',
            'sender' => $infos['connectionInfo']['walletaddress'],
            'receiver' => $infos['receiverConnectionInfo']['walletaddress'],
            'nodoint' => $infos['connectionInfo']['ownerip'],
            'assetqty' => $amount,
            'asset' => 'synk',
            'token' => $infos['totp'],
            'ts' => $infos['timestamp']
        ];

        $request = new SynkronyRequest(
            $this->app,
            'POST',
            $requestBcParams,
            'url'
        );

        $response = $this->getClient()->sendRequest($request, 'base');
        return $response;
    }

    /**
     * Get token
     *
     * @param boolean $transfer
     * @param array $params optional array of parameter
     *
     * @return BaseResponse
     * @throws SynkronySDKException
     */
    protected function getToken($transfer = null, $params = null)
    {
        $endpoint = (isset($transfer) && true === $transfer)? '/api-sdk/auth/token?transfer=true': '/api-sdk/auth/token';
        $request = new SynkronyRequest(
            $this->app,
            ($transfer)? 'POST': 'GET',
            ($transfer)? $params: [],
            ($transfer)? 'json': null
        );

        $request->setHeaders([
            'applicationkey' => $this->getApp()->getAppKey(),
            'applicationid' => $this->getApp()->getAppId()
        ]);
        $response = $this->getClient()->sendRequest($request, 'base', ['endpoint' => $endpoint]);
        return $response;
    }

    /**
     * Check if passed data is an array or a json string and return an encoded version of data.
     *
     * @param mixed $data json|array
     *
     * @return string
     * @throws SynkronySDKException
     */
    protected function checkAndConvertSaveData($data)
    {
        if(is_array($data)) {
            if(empty($data)) {
                throw new SynkronySDKException("Insert a non empty array", null);
            }
            return Utils::encodeArray($data);
        }

        if(is_string($data)) {
            if(!json_decode($data)) {
                throw new SynkronySDKException("Inserti a json string", null);
            }
            return Utils::encodeString($data);
        }

        throw new SynkronySDKException('Insert a string or an array', null);
    }
}
