<?php
/**
 * SynkronyException class
 */

namespace Synkrony\Exceptions;

/**
 * Class SynkronySDKException
 *
 * @package Synkrony\Exceptions
 */
class SynkronySDKException extends \Exception
{
    /**
     *
     * @var mixed $_data Additional exception data
     */
    private $_data = '';

    /**
     * Create new AffidatySDKException
     *
     * @param string    $message    Exception message
     * @param array     $data       Additional data
     */
    public function __construct($message, $data)
    {
        $this->_data = $data;
        parent::__construct($message);
    }

    /**
     * Return exception data if available
     *
     * return mixed
     */
    public function getExceptionData()
    {
        return $this->_data;
    }
}
