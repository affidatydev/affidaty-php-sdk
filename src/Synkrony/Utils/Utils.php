<?php

/**
 * This class contain some utils method shared in the project
 */

namespace Synkrony\Utils;

use Synkrony\SynkronyResponse;

/**
 * Class Utils
 *
 * @package Synkrony
 */
class Utils
{
    /**
     * Encode passed array to hexadecimal string
     *
     * @param array $data
     *
     * @return string Encoded string
     */
    public static function encodeArray($data)
    {
        $jsonString = json_encode($data);
        return Utils::encodeString($jsonString);
    }

    /**
     * Encode passed json to hexadecimal string
     *
     * @param string $string
     *
     * @return string Encoded string
     */
    public static function encodeString($string)
    {
        $unpacked = unpack('H*', $string);
        return array_shift($unpacked);
    }

    /**
     * Decode passed hexadecimal string
     *
     * @param string $string
     *
     * @return string decoded string
     */
    public static function decodeString($string)
    {
        $str = '';
        for($i=0;$i<strlen($string);$i+=2)
            $str .= chr(hexdec(substr($string,$i,2)));

        return $str;
    }

    /**
     * Map the returned data as array of transaction. Id as key and data as value
     *
     * @param SynkronyResponse $response
     *
     * @return array
     */
    public static function prepareReadResult($response)
    {
        $result = [];
        foreach ($response['result'] as $transaction) {
            if(!empty($transaction['data'])) {
                $result[$transaction['txid']] = json_decode(Utils::decodeString($transaction['data'][0]));
            }
        }
        return $result;
    }
}
