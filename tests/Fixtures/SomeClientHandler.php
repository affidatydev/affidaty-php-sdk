<?php

namespace Synkrony\Tests\Fixtures;

use Synkrony\Http\RawResponse;
use Synkrony\HttpClients\HttpClientInterface;

class SomeClientHandler implements HttpClientInterface
{
    public function send($url, $method, $body, array $headers, $timeOut)
    {
        return new RawResponse(
            "HTTP/1.1 200 OK\r\nDate: Tue, 04 Jun 2019 22:47:56 GMT",
            '{"data":[{"id":"1","name":"Asd"},{"id":"2","name":"Lol"}], "error": ""}'
        );
    }
}
