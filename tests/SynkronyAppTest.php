<?php

namespace Synkrony\Tests;

use PHPUnit\Framework\TestCase;
use Synkrony\SynkronyApp;

class SynkronyAppTest extends TestCase
{
    /**
     * @var SynkronyApp
     */
    private $app;
    protected function setUp() :void
    {
        $this->app = new SynkronyApp([
            'application_key' => 'key',
            'application_id' => 'appid',
            'test_url' => '',
            'production_url' => '',
        ]);
    }
    public function testGetKey()
    {
        $this->assertEquals('key', $this->app->getAppKey());
    }

    public function testApplicationId()
    {
        $this->assertEquals('appid', $this->app->getAppId());
    }

    public function testSerialization()
    {
        $newApp = unserialize(serialize($this->app));
        $this->assertInstanceOf('Synkrony\SynkronyApp', $newApp);
        $this->assertEquals('key', $newApp->getAppKey());
        $this->assertEquals('appid', $newApp->getAppId());
    }
}
