<?php

namespace Synkrony\Tests\Responses;

use Synkrony\Responses\BaseResponse;
use Synkrony\SynkronyRequest;

class BaseResponsesTest extends AbstractTestResponse
{
    protected $request;

    protected $body;

    protected $errorBody;

    public function setUp() :void
    {
        parent::setUp();

        $this->request = new SynkronyRequest();
        $this->body = json_encode($this->baseBody);
        $this->errorBody = json_encode($this->baseErrorBody);
    }

    public function testCanConstructClass()
    {
        $base = new  BaseResponse($this->request, $this->body, 200);
        $this->assertInstanceOf(BaseResponse::class, $base);
    }

    public function testCanDecodeBody()
    {
        $base = new BaseResponse($this->request, $this->body, 200);
        $this->assertEquals(json_decode($this->body)->result, $base->getResult());
    }

    public function testCanHaveError()
    {
        $base = new BaseResponse($this->request, $this->errorBody, 200);
        $this->assertTrue($base->isError());
    }
}
