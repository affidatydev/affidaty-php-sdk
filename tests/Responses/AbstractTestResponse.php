<?php

namespace Synkrony\Tests\Responses;

use PHPUnit\Framework\TestCase;

abstract class AbstractTestResponse extends TestCase
{

    /**
     * Base body response absraction
     */
    protected $baseBody = ['result' => 'Test result', 'error' => 'false'];

    /**
     * BALANCE
     *
     * Balance resposne default body absraction
     */
    protected $balanceBody = [
        'disp' => [
            0 => [
                'result' => [
                    0 => [
                        'name' => 'bitbel',
                        'assetref' => '37-266-33001',
                        'qty' => 0.0,
                    ]
                ],
                'error' => '',
                'id' => '',
            ]
        ],
        'cont' => [
            0 => [
                'result' => [
                    0 => [
                        'name' => 'bitbel',
                        'assetref' => '37-266-33001',
                        'qty' => 0.0,
                    ]
                ],
                'error' => '',
                'id' => '',
            ]
        ]
    ];
    /**
     * getResult
     */
    protected $balanceResultBody = [
        'result' => [
            'disp' => [
                'name' => 'bitbel',
                'assetref' => '37-266-33001',
                'qty' => 0.0,
            ],
            'cont' => [
                'name' => 'bitbel',
                'assetref' => '37-266-33001',
                'qty' => 0.0,
            ]
        ]
    ];
    /**
     * getCont
     */
    protected $balanceContBody = [
        'name' => 'bitbel',
        'assetref' => '37-266-33001',
        'qty' => 0.0,
    ];
    /**
     * getDisp
     */
    protected $balanceDispBody = [
        'name' => 'bitbel',
        'assetref' => '37-266-33001',
        'qty' => 0.0,
    ];

    /**
     * TRANSACTION LIST
     */
    protected $transactionList = [
        'result' => [
            0 => [
                'balance' => [
                    'amount' => 0.0,
                    'assets' => [
                        0 => [
                            'name' => 'bitbel',
                            'assetref' => '37-266-33001',
                            'qty' => 0.0
                        ]
                    ]
                ],
                'myaddresses' => [
                    0 => 'zyx'
                ],
                'addresses' => [
                    0 => 'xyz'
                ],
                'permissions' => [],
                'items' => [],
                'data' => [],
                'confirmations' => 123,
                'blockhash' => '0027186934d769358d371b4ce1837a11f9cab4bbb1db60ef11920a1930b83f3a',
                'blockindex' => 3,
                'blocktime' => 1567088805,
                'txid' =>'6fd2c0999fbfde38c1314844076cc04aa95181d66b3546917b4bf92120e5f384',
                'valid' => true,
                'time' => 1567088804,
                'timereceived' => 1567088804
            ],
            1 => [
                'balance' => [
                    'amount' => 0.0,
                    'assets' => [
                        0 => [
                            'name' => 'bitbel',
                            'assetref' => '37-266-33001',
                            'qty' => 0.0
                        ]
                    ]
                ],
                'myaddresses' => [
                    0 => 'zyx'
                ],
                'addresses' => [
                    0 => 'xyz'
                ],
                'permissions' => [],
                'items' => [],
                'data' => [
                    0 => '7b226b6579223a2276616c7565227d'
                ],
                'confirmations' => 321,
                'blockhash' => '001f0c0159023434e5bdb94fdecd0f890eedda4187741b033180f11c57fc4871',
                'blockindex' => 2,
                'blocktime' => 1567088905,
                'txid' =>'355168502a121183f7668fb6e7ac2edfea060ea4c3030cef4ee8246af0a1c755',
                'valid' => true,
                'time' => 1567088904,
                'timereceived' => 1567088904
            ]
        ],
        'error' => '',
        'id' => ''
    ];

    /**
     * SIMPLE RESPONSE
     */
    protected $simpleResponseBody = [
        'result' => [
            'balance' => [
                'amount' => 0.0,
                'assets' => [
                    0 => [
                        'name' => 'bitbel',
                        'assetref' => '37-266-33001',
                        'qty' => 0.0
                    ]
                ]
            ],
            'myaddresses' => [
                0 => 'zyx'
            ],
            'addresses' => [
                0 => 'xyz'
            ],
            'permissions' => [],
            'items' => [],
            'data' => [],
            'confirmations' => 123,
            'blockhash' => '0027186934d769358d371b4ce1837a11f9cab4bbb1db60ef11920a1930b83f3a',
            'blockindex' => 3,
            'blocktime' => 1567088805,
            'txid' =>'6fd2c0999fbfde38c1314844076cc04aa95181d66b3546917b4bf92120e5f384',
            'valid' => true,
            'time' => 1567088804,
            'timereceived' => 1567088804
        ],
        'error' => '',
        'id' => ''
    ];


    /**
     * ERROR BODY
     */
    protected $baseErrorBody = ['result' => 'Something went wrong', 'error' => true];
    protected $balanceErrorBody = [
        'disp' => [
            0 => [
                'result' => [],
                'error' => true,
                'id' => ''
            ]
        ],
        'cont' => [
            0 => [
                'result' => [],
                'error' => '',
                'id' => ''
            ]
        ]
    ];
}
