<?php

namespace Synkrony\Tests\Response;

use Synkrony\Responses\BalanceResponse;
use Synkrony\SynkronyRequest;
use Synkrony\Tests\Responses\AbstractTestResponse;

class BalanceResponseTest extends AbstractTestResponse
{
    protected $request;

    protected $body;

    protected $errorBody;

    public function setUp() :void
    {
        parent::setUp();

        $this->request = new SynkronyRequest();
        $this->body = json_encode($this->balanceBody);
        $this->errorBody = json_encode($this->balanceErrorBody);
    }

    public function testCanConstructClass()
    {
        $balance = new  BalanceResponse($this->request, $this->body, 200);
        $this->assertInstanceOf(BalanceResponse::class, $balance);
    }

    public function testCanDecodeBody()
    {
        $balance = new BalanceResponse($this->request, $this->body, 200);
        $this->assertEquals(json_decode(json_encode($this->balanceBody), true), $balance->getDecodedBody());
        $this->assertEquals(json_decode(json_encode($this->balanceResultBody),true), $balance->getResult());
        $this->assertEquals(json_decode(json_encode($this->balanceContBody),true), $balance->getBalanceAccount());
        $this->assertEquals(json_decode(json_encode($this->balanceDispBody),true), $balance->getBalanceAvailable());
    }

    public function testCanHaveError()
    {
        $balance = new BalanceResponse($this->request, $this->errorBody, 200);
        $this->assertTrue($balance->isError());
    }

}
