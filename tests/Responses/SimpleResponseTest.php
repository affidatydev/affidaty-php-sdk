<?php

namespace Synkrony\Tests\Response;

use Synkrony\Responses\ResponseBody;
use Synkrony\Responses\SimpleResponse;
use Synkrony\SynkronyRequest;
use Synkrony\Tests\Responses\AbstractTestResponse;

class SimpleResponseTest extends AbstractTestResponse
{
    protected $request;

    protected $body;

    protected $errorBody;

    public function setUp() :void
    {
        parent::setUp();

        $this->request = new SynkronyRequest();
        $this->body = json_encode($this->simpleResponseBody);
        $this->errorBody = json_encode($this->baseErrorBody);
    }

    public function testCanConstructClass()
    {
        $simple = new  SimpleResponse($this->request, $this->body, 200);
        $this->assertInstanceOf(SimpleResponse::class, $simple);
    }

    public function testIsResponseBodyOnDecode()
    {
        $simple = new  SimpleResponse($this->request, $this->body, 200);
        $this->assertInstanceOf(ResponseBody::class, $simple->getResult());
    }

    public function testCanGetResponseProperty()
    {
        $simple = new  SimpleResponse($this->request, $this->body, 200);
        // Get a transactions
        $transaction = $simple->getResult();
        // Check properties
        $this->assertIsString($transaction->getTransactionId());
        $this->assertIsInt($transaction->getConfirmations());
        if($transaction->getData())
            $this->assertIsObject($transaction->getData());
        $this->assertIsArray($transaction->getBalance());
        $this->assertIsBool($transaction->getValid());
    }

    public function testCanHaveError()
    {
        $simple = new SimpleResponse($this->request, $this->errorBody, 200);
        $this->assertTrue($simple->isError());
    }
}
