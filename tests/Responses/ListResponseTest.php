<?php

namespace Synkrony\Tests\Response;

use Synkrony\Responses\ListResponse;
use Synkrony\Responses\ResponseBody;
use Synkrony\SynkronyRequest;
use Synkrony\Tests\Responses\AbstractTestResponse;

class ListResponseTest extends AbstractTestResponse
{
    protected $request;

    protected $body;

    protected $errorBody;

    public function setUp() :void
    {
        parent::setUp();

        $this->request = new SynkronyRequest();
        $this->body = json_encode($this->transactionList);
        $this->errorBody = json_encode($this->baseErrorBody);
    }

    public function testCanConstructClass()
    {
        $list = new  ListResponse($this->request, $this->body, 200);
        $this->assertInstanceOf(ListResponse::class, $list);
    }

    public function testIsAListOfResponseBody()
    {
        $list = new ListResponse($this->request, $this->body, 200);
        $this->assertContainsOnly(ResponseBody::class, $list->getResult());
    }

    public function testCanGetResponseProperty()
    {
        $list = new ListResponse($this->request, $this->body, 200);
        // Get a transactions
        $results = $list->getResult();
        $transaction = array_pop($results);
        // Check properties
        $this->assertIsString($transaction->getTransactionId());
        $this->assertIsInt($transaction->getConfirmations());
        $this->assertIsArray($transaction->getData());
        $this->assertIsArray($transaction->getBalance());
        $this->assertIsBool($transaction->getValid());
    }

    public function testCanHaveError()
    {
        $list = new ListResponse($this->request, $this->errorBody, 200);
        $this->assertTrue($list->isError());
    }
}
