<?php

namespace Synkrony\Tests;

use PHPUnit\Framework\TestCase;
use Synkrony\SynkronyApp;
use Synkrony\SynkronyRequest;
use Synkrony\SynkronyClient;
use Synkrony\Tests\Fixtures\SomeClientHandler;

class SynkronyClientTest extends TestCase
{
    /**
     * @var SynkronyApp
     */
    public $synkronyApp;
    /**
     * @var SynkronyClient
     */
    public $synkronyClient;

    protected function setUp() :void
    {
        $this->synkronyApp = new SynkronyApp([
            'application_key' => 'key',
            'application_id' => 'id',
            'test_url' => '',
            'production_url' => '',
        ]);
        $this->synkronyClient = new SynkronyClient(new SomeClientHandler());
    }
    public function testACustomHttpClientCanBeInjected()
    {
        $handler = new SomeClientHandler();
        $client = new SynkronyClient($handler);
        $httpHandler = $client->getHttpClientHandler();
        $this->assertInstanceOf('Synkrony\Tests\Fixtures\SomeClientHandler', $httpHandler);
    }
    public function testASynkronyRequestEntityCanBeUsedToSendARequest()
    {
        $synkronyRequest = new SynkronyRequest($this->synkronyApp, 'POST', ['a' => '1'], 'url');
        $response = $this->synkronyClient->sendRequest($synkronyRequest, 'base');
        $this->assertInstanceOf('Synkrony\SynkronyResponse', $response);
        $this->assertEquals(200, $response->getHttpStatusCode());
        $this->assertEquals('{"data":[{"id":"1","name":"Asd"},{"id":"2","name":"Lol"}], "error": ""}', $response->getBody());
    }
    public function testARequestOfParamsWillBeUrlEncoded()
    {
        $synkronyRequest = new SynkronyRequest($this->synkronyApp, 'POST', ['a' => '1'], 'url');
        $response = $this->synkronyClient->sendRequest($synkronyRequest, 'base');
        $headersSent = $response->getRequest()->getHeaders();
        $this->assertEquals('application/x-www-form-urlencoded', $headersSent['Content-Type']);
    }
    public function testARequestOfParamsWillBeJsonEncoded()
    {
        $synkronyRequest = new SynkronyRequest($this->synkronyApp, 'POST', ['a' => '1'], 'json');
        $response = $this->synkronyClient->sendRequest($synkronyRequest, 'base');
        $headersSent = $response->getRequest()->getHeaders();
        $this->assertEquals('application/json', $headersSent['Content-Type']);
    }
}
