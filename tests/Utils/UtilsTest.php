<?php

namespace Synkrony\Tests\Utils;

use PHPUnit\Framework\TestCase;
use Synkrony\Utils\Utils;

class UtilsTest extends TestCase
{
    protected $decodedArray = ['key' => 'value'];
    protected $decodedString = '';
    protected $encodedString = '7b226b6579223a2276616c7565227d';

    public function setUp() :void
    {
        $this->decodedString = json_encode($this->decodedArray);
    }

    public function testCanEncodeString ()
    {
        $encoded = Utils::encodeString($this->decodedString);
        $this->assertEquals($this->encodedString, $encoded);
    }

    public function testCanEncodeArray ()
    {
        $encoded = Utils::encodeArray($this->decodedArray);
        $this->assertEquals($this->encodedString, $encoded);
    }

    public function testCanDecodeEncodedString ()
    {
        $this->assertEquals(json_decode($this->decodedString), json_decode(Utils::decodeString($this->encodedString)));
    }
}
