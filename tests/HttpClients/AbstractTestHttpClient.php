<?php

namespace Synkrony\Tests\HttpClients;

use PHPUnit\Framework\TestCase;

abstract class AbstractTestHttpClient extends TestCase
{
    protected $fakeRawRedirectHeader = "HTTP/1.1 302 Found
Content-Type: text/html; charset=utf-8
Location: https://synkrony.io/\r\n\r\n";

    protected $fakeRawProxyHeader = "HTTP/1.0 200 Connection established\r\n\r\n";

    protected $fakeRawProxyHeader2 = "HTTP/1.0 200 Connection established
Proxy-agent: Kerio Control/7.1.1 build 1971\r\n\r\n";

    protected $fakeRawHeader = "HTTP/1.1 200 OK
Content-Type: text/javascript; charset=UTF-8
Pragma: no-cache
Expires: Sat, 01 Jan 2000 00:00:00 GMT
Connection: close
Date: Mon, 19 Oct 2018 12:34:57 GMT
Cache-Control: private, no-cache, no-store, must-revalidate
Access-Control-Allow-Origin: *\r\n\r\n";
    protected $fakeRawBody = "{\"id\":\"123\",\"name\":\"Foo Bar\"}";
    protected $fakeHeadersAsArray = [
        'Content-Type' => 'text/javascript; charset=UTF-8',
        'Pragma' => 'no-cache',
        'Expires' => 'Sat, 01 Jan 2000 00:00:00 GMT',
        'Connection' => 'close',
        'Date' => 'Mon, 19 Oct 2018 12:34:57 GMT',
        'Cache-Control' => 'private, no-cache, no-store, must-revalidate',
        'Access-Control-Allow-Origin' => '*',
    ];
}
