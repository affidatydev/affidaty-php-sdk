<?php

namespace Synkrony\Tests\HttpClients;

use PHPUnit\Framework\TestCase;
use Synkrony\HttpClients\CurlHttpClient;
use Synkrony\HttpClients\HttpClientsFactory;

class HttpClientsFactoryTest extends TestCase
{
    const COMMON_NAMESPACE = 'Synkrony\HttpClients\\';
    const COMMON_INTERFACE = 'Synkrony\HttpClients\HttpClientInterface';
    /**
     * @param mixed  $handler
     * @param string $expected
     *
     * @dataProvider httpClientsProvider
     */
    public function testCreateHttpClient($handler, $expected)
    {
        $httpClient = HttpClientsFactory::createHttpClient($handler);
        $this->assertInstanceOf(self::COMMON_INTERFACE, $httpClient);
        $this->assertInstanceOf($expected, $httpClient);
    }
    /**
     * @return array
     */
    public function httpClientsProvider()
    {
        $clients = [];
        if (extension_loaded('curl')) {
            $clients[] = ['curl', self::COMMON_NAMESPACE . 'CurlHttpClient'];
            $clients[] = [new CurlHttpClient(), self::COMMON_NAMESPACE . 'CurlHttpClient'];
        }
        return $clients;
    }
}
