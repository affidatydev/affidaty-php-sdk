<?php

namespace Synkrony\Tests\HttpClients;

use function foo\func;
use Mockery as m;
use Synkrony\Exceptions\SynkronySDKException;
use Synkrony\HttpClients\Curl;
use Synkrony\HttpClients\CurlHttpClient;

class CurlHttpClientTest extends AbstractTestHttpClient
{
    /**
     * @var Curl
     */
    protected $curlMock;
    /**
     * @var CurlHttpClient
     */
    protected $curlClient;

    protected function setUp() :void
    {
        if (!extension_loaded('curl')) {
            $this->markTestSkipped('cURL must be installed to test cURL client handler.');
        }
        $this->curlMock = m::mock('Synkrony\HttpClients\Curl');
        $this->curlClient = new CurlHttpClient($this->curlMock);
    }

    public function tearDown() :void
    {
        m::close();
    }

    public function testCanOpenGetCurlConnection()
    {
        $this->curlMock
            ->shouldReceive('init')
            ->once()
            ->andReturn(null);
        $this->curlMock
            ->shouldReceive('setoptArray')
            ->once()
            ->andReturn(null);
        $this->curlClient->openConnection('https://synkrony.io', 'GET', 'body', [], 123);
        $this->assertTrue(true);
    }
    public function testCanOpenCurlConnectionWithPostBody()
    {
        $this->curlMock
            ->shouldReceive('init')
            ->once()
            ->andReturn(null);
        $this->curlMock
            ->shouldReceive('setoptArray')
            ->once()
            ->andReturn(null);
        $this->curlClient->openConnection('https://synkrony.io', 'POST', 'arg1=par1', [], 60);
        $this->assertTrue(true);
    }
    public function testCanCloseConnection()
    {
        $this->curlMock
            ->allows()
            ->close()
            ->andReturns(null);
        $this->curlClient->closeConnection();
        $this->assertTrue(true);
    }
    public function testIsolatesTheHeaderAndBody()
    {
        $this->curlMock
            ->shouldReceive('exec')
            ->once()
            ->andReturn($this->fakeRawHeader . $this->fakeRawBody);
        $this->curlClient->sendRequest();

        list($rawHeader, $rawBody) = $this->curlClient->extractResponseHeadersAndBody();
        $this->assertEquals($rawHeader, trim($this->fakeRawHeader));
        $this->assertEquals($rawBody, $this->fakeRawBody);
    }
    public function testProperlyHandlesRedirectHeaders()
    {
        $rawHeader = $this->fakeRawRedirectHeader . $this->fakeRawHeader;
        $this->curlMock
            ->shouldReceive('exec')
            ->once()
            ->andReturn($rawHeader . $this->fakeRawBody);
        $this->curlClient->sendRequest();

        list($rawHeaders, $rawBody) = $this->curlClient->extractResponseHeadersAndBody();
        $this->assertEquals($rawHeaders, trim($rawHeader));
        $this->assertEquals($rawBody, $this->fakeRawBody);
    }
    public function testCanSendNormalRequest()
    {
        $this->curlMock
            ->shouldReceive('init')
            ->once()
            ->andReturn(null);
        $this->curlMock
            ->shouldReceive('setoptArray')
            ->once()
            ->andReturn(null);
        $this->curlMock
            ->shouldReceive('exec')
            ->once()
            ->andReturn($this->fakeRawHeader . $this->fakeRawBody);
        $this->curlMock
            ->shouldReceive('errno')
            ->once()
            ->andReturn(null);
        $this->curlMock
            ->shouldReceive('close')
            ->once()
            ->andReturn(null);
        $response = $this->curlClient->send('https://synkrony.io/', 'GET', '', [], 60);

        $this->assertInstanceOf('Synkrony\Http\RawResponse', $response);
        $this->assertEquals($this->fakeRawBody, $response->getBody());
        $this->assertEquals($this->fakeHeadersAsArray, $response->getHeaders());
        $this->assertEquals(200, $response->getHttpResponseCode());
    }

    public function testThrowsExceptionOnClientError()
    {
        $this->expectException(SynkronySDKException::class);
        $this->curlMock
            ->shouldReceive('init')
            ->once()
            ->andReturn(null);
        $this->curlMock
            ->shouldReceive('setoptArray')
            ->once()
            ->andReturn(null);
        $this->curlMock
            ->shouldReceive('exec')
            ->once()
            ->andReturn(null);
        $this->curlMock
            ->shouldReceive('errno')
            ->once()
            ->andReturn(null);
        $this->curlMock
            ->shouldReceive('close')
            ->once()
            ->andReturnUsing(function () {
                throw new SynkronySDKException('error', null);
            });
        $this->curlClient->send('https://synkrony.io/asd', 'GET', '', [], 60);
    }
}
