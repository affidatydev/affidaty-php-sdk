<?php

namespace Synkrony\Tests;

use PHPUnit\Framework\TestCase;
use Synkrony\Exceptions\SynkronySDKException;
use Synkrony\Synkrony;

class SynkronyTest extends TestCase
{
    protected $config = [
        'application_key' => 'key',
        'application_id' => 'id',
        'test_url' => '',
        'production_url' => '',
        'test_mode' => true,
    ];

    public function testInstantiatingWithoutAppKeyThrows()
    {
        $this->expectException(SynkronySDKException::class);
        $config = [
            'application_key' => null,
            'application_id' => 'id',
            'test_url' => ''
        ];
        new Synkrony($config);
    }

    public function testInstantiatingWithoutAppIdThrows()
    {
        $this->expectException(SynkronySDKException::class);
        $config = [
            'application_key' => 'key',
            'application_id' => null,
            'test_url' => '',
            'production_url' => ''
        ];
        new Synkrony($config);
    }

    public function testSettingAnInvalidHttpClientHandlerThrows()
    {
        $this->expectException(\InvalidArgumentException::class);
        $config = array_merge($this->config, [
            'http_client_handler' => 'invalid_handler',
        ]);
        new Synkrony($config);
    }
    public function testCurlHttpClientHandlerCanBeForced()
    {
        if (!extension_loaded('curl')) {
            $this->markTestSkipped('cURL must be installed to test cURL client handler.');
        }
        $config = array_merge($this->config, [
            'http_client_handler' => 'curl'
        ]);
        $synkrony = new Synkrony($config);
        $this->assertInstanceOf(
            'Synkrony\HttpClients\CurlHttpClient',
            $synkrony->getClient()->getHttpClientHandler()
        );
    }
}
