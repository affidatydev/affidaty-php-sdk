<?php

namespace Synkrony\Tests;

use PHPUnit\Framework\TestCase;
use Synkrony\Exceptions\SynkronySDKException;
use Synkrony\SynkronyApp;
use Synkrony\SynkronyRequest;

class SynkronyRequestTest extends TestCase
{
    protected $config;

    public function setUp() :void
    {
        $this->config = [
            'application_key' => '42',
            'application_id' => 'XYZ',
            'test_url' => '',
            'production_url' => '',
        ];
    }

    public function testAnEmptyRequestEntityCanInstantiate()
    {
        $app = new SynkronyApp($this->config);
        $request = new SynkronyRequest($app);
        $this->assertInstanceOf('Synkrony\SynkronyRequest', $request);
    }

    public function testAMissingMethodWillThrow()
    {
        $this->expectException(SynkronySDKException::class);
        $app = new SynkronyApp($this->config);
        $request = new SynkronyRequest($app);
        $request->validateMethod();
    }

    public function testAnInvalidMethodWillThrow()
    {
        $this->expectException(SynkronySDKException::class);
        $app = new SynkronyApp($this->config);
        $request = new SynkronyRequest($app, 'PUT', ['a' => 1]);
        $request->validateMethod();
    }
}
