<?php

namespace Synkrony\Tests;

use PHPUnit\Framework\TestCase;
use Synkrony\SynkronyApp;
use Synkrony\SynkronyRequest;
use Synkrony\SynkronyResponse;

class SynkronyResponseTest extends TestCase
{
    /**
     * @var \Synkrony\SynkronyRequest
     */
    protected $request;
    protected function setUp() :void
    {
        $app = new SynkronyApp(['application_key' =>'123', 'application_id' => 'XYZ', 'test_url' => '', 'production_url' => '']);
        $this->request = new SynkronyRequest(
            $app,
            'POST',
            ['foo' => 'bar'],
            'url'
        );
    }
    public function testASuccessfulJsonResponseWillBeDecoded()
    {
        $responseJson = '{"error":false,"id":"1","name":"Asd"}';
        $response = new SynkronyResponse($this->request, $responseJson, 200);
        $decodedResponse = $response->getDecodedBody();
        $this->assertFalse($response->isError(), 'Did not expect Response to return an error.');
        $this->assertEquals([
            'error' => false,
            'id' => '1',
            'name' => 'Asd',
        ], $decodedResponse);
    }
    public function testErrorStatusCanBeCheckedWhenAnErrorResponseIsReturned()
    {
        $responseMessage = '{"error":true, "message":"Qualquadra non cosa", "exceptionData": []}';
        $response = new SynkronyResponse($this->request, $responseMessage, 404);
        $exception = $response->getThrownException();
        $this->assertTrue($response->isError(), 'Expected Response to return an error.');
        $this->assertInstanceOf('Synkrony\Exceptions\SynkronySDKException', $exception);
    }
}
