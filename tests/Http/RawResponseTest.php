<?php

namespace Synkrony\Tests\Http;

use PHPUnit\Framework\TestCase;
use Synkrony\Http\RawResponse;

class RawResponseTest extends TestCase
{

    protected $fakeRawHeader = <<<HEADER
HTTP/1.1 200 OK
Content-Type: text/javascript; charset=UTF-8
Access-Control-Allow-Origin: *\r\n\r\n
HEADER;
    protected $fakeHeadersAsArray = [
        'Content-Type' => 'text/javascript; charset=UTF-8',
        'Access-Control-Allow-Origin' => '*'
    ];


    public function testCanSetTheHeadersFromAnArray()
    {
        $myHeaders = [
            'foo' => 'bar',
            'baz' => 'faz',
        ];
        $response = new RawResponse($myHeaders, '');
        $headers = $response->getHeaders();
        $this->assertEquals($myHeaders, $headers);
    }
    public function testCanSetTheHeadersFromAString()
    {
        $response = new RawResponse($this->fakeRawHeader, '');
        $headers = $response->getHeaders();
        $httpResponseCode = $response->getHttpResponseCode();
        $this->assertEquals($this->fakeHeadersAsArray, $headers);
        $this->assertEquals(200, $httpResponseCode);
    }

    public function testHttpResponseCode()
    {
        // HTTP/1.0
        $headers = str_replace('HTTP/1.1', 'HTTP/1.0', $this->fakeRawHeader);
        $response = new RawResponse($headers, '');
        $this->assertEquals(200, $response->getHttpResponseCode());

        // HTTP/1.1
        $response = new RawResponse($this->fakeRawHeader, '');
        $this->assertEquals(200, $response->getHttpResponseCode());

        // HTTP/2
        $headers = str_replace('HTTP/1.1', 'HTTP/2', $this->fakeRawHeader);
        $response = new RawResponse($headers, '');
        $this->assertEquals(200, $response->getHttpResponseCode());
    }
}
