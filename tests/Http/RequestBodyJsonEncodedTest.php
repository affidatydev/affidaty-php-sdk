<?php

namespace Synkrony\Tests\Http;

use PHPUnit\Framework\TestCase;
use Synkrony\Http\RequestBodyJsonEncoded;

class RequestBodyJsonEncodedTest extends TestCase
{
    public function testCanProperlyEncodeAnArrayOfParams()
    {
        $params = [
            'arg1' => 'param1',
            'qualcosa' => 'Qualquadra non cosa.',
        ];

        $message = new RequestBodyJsonEncoded($params);
        $body = $message->getBody();
        $this->assertEquals(json_encode($params), $body);
    }
    public function testSupportsMultidimensionalParams()
    {
        $params = [
            'test' => 'testino',
            'vettorone' => [1,2,3],
            'some_data' => [
                'city' => 'Rome',
                'country' => 'Italy',
            ]
        ];

        $message = new RequestBodyJsonEncoded($params);
        $body = $message->getBody();
        $this->assertEquals(json_encode($params), $body);
    }
}
