<?php

namespace Synkrony\Tests\Http;

use PHPUnit\Framework\TestCase;
use Synkrony\Http\RequestBodyUrlEncoded;

class RequestUrlEncodedTest extends TestCase
{
    public function testCanProperlyEncodeAnArrayOfParams()
    {
        $message = new RequestBodyUrlEncoded([
            'arg1' => 'param1',
            'qualcosa' => 'Qualquadra non cosa.',
        ]);
        $body = $message->getBody();
        $this->assertEquals('arg1=param1&qualcosa=Qualquadra+non+cosa.', $body);
    }
    public function testSupportsMultidimensionalParams()
    {
        $message = new RequestBodyUrlEncoded([
            'test' => 'testino',
            'vettorone' => [1,2,3],
            'some_data' => [
                'city' => 'Rome',
                'country' => 'Italy',
            ]
        ]);
        $body = $message->getBody();
        $this->assertEquals('test=testino&vettorone%5B0%5D=1&vettorone%5B1%5D=2&vettorone%5B2%5D=3&some_data%5Bcity%5D=Rome&some_data%5Bcountry%5D=Italy', $body);
    }
}
