# Synkrony PHP SDK

This repository contains the Synkrony SDK, which allows you to perform some actions in a blockchain environment.
## Installation

### composer
The sdk package can be downloaded via composer.

First edit the composer.json and add the following line in the require section:
```
    "require": {
    ...
        "affidaty/synkrony-php-sdk": "0.*",
    ...
    },
```
Still in composer.json add references to the repository:
```
    ...
    "repositories": 
        [
            {
                "type": "vcs",
                "url": "https://bitbucket.org/affidatydev/affidaty-php-sdk.git"
            }
        ]
    ...

```

After that, run composer update:
```
$ composer install
```

## Usage

> **Note:** Synkrony SDK require PHP 7.2 or greater.

The information 'application_key', 'application_id' and 'production_url' are available on the portal [developer.affidaty.io](https://developer.affidaty.io)

Simple request for wallet status.


```php
require_once __DIR__ . '/vendor/autoload.php'; // change the path with your own

$synkrony = new \Synkrony\Synkrony([
  'application_key' => '123456',
  'application_id' => '789012',
  'http_client_handler' => 'curl',
  'production_url' => 'https://production.url'
]);

try {
  // We try to ask for information on the state of the wallet
  $response = $synkrony->balance();
} catch(\Synkrony\Exceptions\SynkronySDKException $e) {
  // When we get an error
  echo 'Error: ' . $e->getMessage();
  exit;
}

$balance = $response->getDecodedBody();
```

## Available methods 

#### balances()

This method allows to have information on the available and accounting balance of the wallet passed as a configuration.
```
try {
    $balance = $synkrony->balances();
} catch (\Synkrony\Exceptions\SynkronySDKException $exception) {
    die($exception->getMessage());
}

// To get the complete account information
$response = $balance->getResult();

// To get only the part of the account balance 
$contabile = $balance->getBalanceAccount();

// To get only the part of the available balance
$disponibile = $balance->getBalanceAvailable();
```

#### details(string $txid)
This method allows obtaining information relating to a transaction made through the wallet passed as a configuration.
```
try {
    $details = $synkrony->details('transactionID');
} catch (\Synkrony\Exceptions\SynkronySDKException $exception) {
    die($exception->getMessage());
}

// To get the complete information
$response = $details->getDecodedBody();

// The following methods are available to obtain partial information
$response = $details->getResult()->getBalance();
$response = $details->getResult()->getItems();
$response = $details->getResult()->getData();
$response = $details->getResult()->getConfirmations();
$response = $details->getResult()->getTransactionId();
$response = $details->getResult()->getValid();
```

#### transfers(string $sender, float $amount, string $asset)
This method allows you to transfer a quantity of an asset from one wallet to another.
```
try {
    $transfer = $synkrony->transfers('sender', 3.0, 'assetName');
} catch (\Synkrony\Exceptions\SynkronySDKException $exception) {
    die($exception->getMessage());
}

// If the method has not raised errors
if(!$transfer->isError()) {
    // We get the transaction hash 
    $response = $transfer->getResult();
}
```
#### transactions(int $rows = 10, int $skip = 0)

This method allows obtaining the list of transactions. It is possible to set the number of transactions to be obtained and an offset. If no parameters are passed, the default ones are 10 lines and offset 0.

> **Note:** The transactions are returned as a vector of ResponseBody, so the methods described for the details call are available.

> **Note:** This method can also be used to obtain information saved on transactions using the method **save(string $json)**.

```
try {
    $transactions = $synkrony->transactions(15, 5);
} catch (\Synkrony\Exceptions\SynkronySDKException $exception) {
    die($exception->getMessage());
}

// If the method has not raised errors
if(!$transactions->isError()) {
    // Let's look at the list of transactions
    foreach($transactions->getResults() as $transaction) {
        // We get the hash of a transaction
        $txid = $transaction->getTransactionId();
    }
}
```
#### save(string $json)
This method allows to save a json of data.
```
// Create a json string
$json = json_encode(['foo' => 'bar']);

try {
    $save = $synkrony->save($json);
} catch (\Synkrony\Exceptions\SynkronySDKException $exception) {
    die($exception->getMessage());
}

// If the method has not raised errors
if(!$save->isError()) {
    // We get the hash of a transaction 
    $response = $save->getResult();
}

```
#### register(array $userData)
This method allows you to register a new user. The new user will be referenced to the developer making the registration call.

```
// Create the user data array
$userData = [
    'name' => 'Jhon',
    'surname' => 'Doe',
    'email' => 'json.doe@someprovider.com',
    'phone' => '+39 1234567890',
    'password' => 'Secure password'
]

$registration = $synkrony->register($userData);

// If the method has not raised errors
if(!$registration->isError()) {
    // We get the server response
    $response = $registration->getDecodedBody();
}

```
## Tests
To run the tests we must have phpunit installed in the project vendors. If the package is not present, install it with composer:
```
$ composer require phpunit/phpunit
``` 
To perform the complete test suite perform:

```bash
$ ./vendor/bin/phpunit
```

To run a single test:

```bash
$ ./vendor/bin/phpunit tests/SynkronyTest.php
```
## Documentation

The documentation of this project is updated at [developer.affidaty.io](https://developer.affidaty.io).

It can be regenerated using phpDocumentor.

```
$ phpdoc run -d src/ -t documentations/ --template="clean" --title="Synkrony PHP SDK"
```

